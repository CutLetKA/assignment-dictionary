all: main.o lib.o dict.o
	ld main.o lib.o dict.o -o lab2

%.o: %.asm
	nasm -felf64 -o $@ $<

.PHONY: clean

clean:
	rm -rf *.o lab2
