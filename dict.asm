%define OFFSET 8

section .text
global find_word

extern string_equals
				; Параметры: (rsi, rdi)
				; rsi - адрес начала связного списка
				; rdi - указатель на нуль-терминированную строку (ключ)
				; Возвращает: (rax)
				; rax - адрес начала вхождения найденного элемента, либо 0 при его отсутствии

find_word:
.loop:
	push rsi		; 
	add rsi, OFFSET		; 
	call string_equals	; Сравниваем значение со строкой ключа
	pop rsi		; 
	cmp rax, 0 		; Проверка на соответствие
	jne .have 		; 
	mov rsi, [rsi] 	; Переходим к предыдущему элементу
	cmp rsi, 0 		; Если предыдущего элемента нет,
	je .have_not		; то завершаем поиск
	jmp .loop 		; 
.have:
    mov rax, rsi 		; rax = адрес начала вхождения 
    ret 			;
.have_not:
    xor rax, rax 		; 
    ret			;
