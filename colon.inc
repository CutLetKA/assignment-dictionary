%define current 0						; В current всегда адрес начала списка
								; %1_key - ключ
								; %2_name - значение
%macro colon 2		
	%ifstr %%1_key						;
		db %%1_key, 0					; Запись ключа
	%else							;
		%fatal "Первый элемент не Строка"		;
	%endif		
								; 
	%ifid %2_name						;
		%2_name: dq current				; Связываем с предыдущим
		%define current %2				; Обновляем current
	%else							;
		%fatal "Второй элемент не ID" 		;
	%endif							;	
								; 	
%endmacro							;
